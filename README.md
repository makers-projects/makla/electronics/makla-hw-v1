# makla-hw-v1

The hardware for the first fully functional version of the *makla* project, consisting of two boards:

- The **Processing board** for keyboard IO and main processing
- The **Audio board** for audio synthesis and output

The overarching documentation of the project is located in the [makla-meta](https://gitlab.com/makers-projects/makla/makla-meta) repo.
